global beta h alpha ja D
beta = -1;
h = 1.2;
alpha = 0.02;

%j = 1.0;
%ja=j/alpha;
ja=2.4;
x=0:0.01*pi:1.0*pi;
y=cos(x);

z1=sin(x);
%z=exp(-2*alpha*(j/alpha-h)/D*(0.5*log((1-x)./(1+x))) - 2*beta*log(0.5*((1-x)./(1+x))^0.5+((1+x)./(1-x))^0.5)/D)/(1-x.*x);
%%z=exp(-2*alpha*(j./alpha-h)*(0.5*log((1-x)./(1+x)))/D - 
%2*beta*log(0.5*(((1-x)./(1+x))^0.5+((1+x)./(1-x))^0.5))./D)/(1-x.*x);
figure (1)
plot(x,z1,'-k'),xlabel('\beta/\alpha'),ylabel('\omega');

% figure(3)
% z3=1./(1-x.*x);
% z2=exp(+ 2*(alpha*beta/D)*log(cosh(y)));
% 
% 
% %- 2*beta*log(0.5*((1-x)./(1+x))^0.5+((1+x)./(1-x))^0.5)/D)/(1-x.*x);
% plot(x,z2,'m'),xlabel('\beta/\alpha'),ylabel('\omega');
D = 0.0001;
z2=exp((alpha/(beta*D))*((ja-h-beta*cos(x)).*(ja-h-beta*cos(x))));
figure (2)
plot(x,z2,'b'),xlabel('\beta/\alpha'),ylabel('\omega');
hold on;
Q = quad(@pr_dens_hns,0,pi);
figure (4)
plot(x/pi,z1.*z2/Q,'b','LineWidth',2),xlabel('\theta/\pi'),ylabel('W(\theta)');
hold on;

D = 0.001;
z2=exp((alpha/(beta*D))*((ja-h-beta*cos(x)).*(ja-h-beta*cos(x))));
figure (2)
plot(x,z2,'b'),xlabel('\beta/\alpha'),ylabel('\omega');
hold on;
Q = quad(@pr_dens_hns,0,pi);
figure (4)
plot(x/pi,z1.*z2/Q,'c','LineWidth',2),xlabel('\theta/\pi'),ylabel('W(\theta)');
hold on;

D = 0.01;
z2=exp((alpha/(beta*D))*((ja-h-beta*cos(x)).*(ja-h-beta*cos(x))));
figure (2)
plot(x,z2,'b'),xlabel('\beta/\alpha'),ylabel('\omega');
hold on;
Q = quad(@pr_dens_hns,0,pi);
figure (4)
plot(x/pi,z1.*z2/Q,'g','LineWidth',2),xlabel('\theta/\pi'),ylabel('W(\theta)');
hold on;

D = 0.1;
z2=exp((alpha/(beta*D))*((ja-h-beta*cos(x)).*(ja-h-beta*cos(x))));
figure (2)
plot(x,z2,'b'),xlabel('\beta/\alpha'),ylabel('\omega');
hold on;
Q = quad(@pr_dens_hns,0,pi);
f4=figure (4)
plot(x/pi,z1.*z2/Q,'m','LineWidth',2),xlabel('\theta/\pi','FontSize',20),ylabel('W(\theta)','FontSize',20);
hold on;
% a1 = axes(f4, 'FontWeight','bold', 'FontSize',24);




%F = inline('sin(x).*exp((-0.02/(0.0001))*((2.4-1.2+cos(x)).*(2.4-1.2+cos(x))))'); 
%Q = quad(F,0,pi); 
Q = quad(@pr_dens_hns,0,pi) 