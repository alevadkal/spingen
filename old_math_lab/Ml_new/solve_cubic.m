clear;
global alfa;
global k;
global beta;
global h;
global D;
k = -1;
h = 1.2;
alfa = 0.02;
beta = -0.02;
i = 1;
x0 = 1.;
D = 0.1;
while beta < 0.075
    x(i) = fzero('f3',x0);
    x0 = x(i);    
    bet(i) = beta;
    beta = beta + 0.001;
    i = i + 1; 
end
% figure(1)
% hold on;
% plot(bet/alfa,x,'k'),xlabel('\beta/\alpha'),ylabel('cos(\theta)');
% figure(2)
% S = -alfa*(h+k*x-bet/alfa).*x/(1+alfa^2)+alfa*k*(1-x.^2)/(1+alfa^2)-D./(1-x.^2);
% plot(bet/alfa,S); grid on;
figure(3)
Omg = h + k*x;

D = 0.01;
while beta < 0.075
    x(i) = fzero('f3',x0);
    x0 = x(i);    
    bet(i) = beta;
    beta = beta + 0.001;
    i = i + 1; 
end
%figure(3)
Omg2 = h + k*x;
plot(bet/alfa,Omg2,'g'),'LineWidth',2,xlabel('\beta/\alpha'),ylabel('\omega');
grid on
hold on;

D = 0.001;
while beta < 0.075
    x(i) = fzero('f3',x0);
    x0 = x(i);    
    beta = beta + 0.001;
    i = i + 1; 
end
%figure(3)
Omg3 = h + k*x;
plot(bet/alfa,Omg,'m--'),'LineWidth',2,xlabel('\beta/\alpha'),ylabel('\omega');
hold on;
plot(bet/alfa,Omg3,'c--'),'LineWidth',2,xlabel('\beta/\alpha'),ylabel('\omega');


grid on

D = 0.0001;
while beta < 0.075
    x(i) = fzero('f3',x0);
    x0 = x(i);    
    bet(i) = beta;
    beta = beta + 0.001;
    i = i + 1; 
end
%figure(3)
Omg4 = h + k*x;
plot(bet/alfa,Omg4,'b--'),'LineWidth',2,xlabel('\beta/\alpha'),ylabel('\omega');
grid on
hold on;

% [filename,path]=uigetfile('*.*','data file');
% fullfilename=strcat(path,filename);
% fid=fopen(fullfilename,'rt');
% %n=inputdlg('Please, input number of columns','Quastion',4,{'4'});
% %n=str2num(n{1});
% n=7;
% [w]=fscanf(fid,'%f',[n,inf]);
% plot(w(1,:),w(2,:),'r*','MarkerSize',10);
