function y = mean_omg_hns(x) 
global beta h alpha ja D 
y = exp((alpha/(beta*D))*((ja-h-beta*cos(x)).*(ja-h-beta*cos(x)))).*sin(x).*cos(x);

