global beta h alpha ja D
beta = -1;
h = 1.2;
alpha = 0.02;

D = 0.1;
k=0;
j_a=zeros(30,1);
m_om=zeros(30,1);
for ja = -0.5: +0.1: 2.5
    k = k+1;
    j_a(k)=ja;
    m_om(k) = quadl(@mean_omg_hns,0,pi,1E-9)/quadl(@pr_dens_hns,0,pi,1E-9);
end

plot(j_a,h+beta*m_om,'m','LineWidth',2),xlabel('\beta/\alpha','FontSize',20),ylabel('\langle \omega \rangle','FontSize',20);
hold on;

D = 0.01;
k=0;
j_a=zeros(30,1);
m_om=zeros(30,1);
for ja = -0.5: +0.1: 2.5
    k = k+1;
    j_a(k)=ja;
    m_om(k) = quadl(@mean_omg_hns,0,pi,1E-9)/quadl(@pr_dens_hns,0,pi,1E-9);
end

plot(j_a,h+beta*m_om,'g','LineWidth',2),xlabel('\beta/\alpha','FontSize',20),ylabel('\langle \omega \rangle','FontSize',20);
hold on;

D = 0.001;
k=0;
j_a=zeros(30,1);
m_om=zeros(30,1);
for ja = -0.5: +0.1: 2.5
    k = k+1;
    j_a(k)=ja;
    m_om(k) = quadl(@mean_omg_hns,0,pi,1E-17)/quadl(@pr_dens_hns,0,pi,1E-17);
end

plot(j_a,h+beta*m_om,'c','LineWidth',2),xlabel('\beta/\alpha','FontSize',20),ylabel('\langle \omega \rangle','FontSize',20);
hold on;


D = 0.0001;
k=0;
j_a=zeros(27,1);
m_om=zeros(27,1);
for ja = -0.2: 0.1: 2.5
    k = k+1;
    j_a(k)=ja;
    m_om(k) = quadl(@mean_omg_hns,0,pi,1E-18)/quadl(@pr_dens_hns,0,pi,1E-18);
end

plot(j_a,h+beta*m_om,'b','LineWidth',2),xlabel('\beta/\alpha'),ylabel('\langle \omega \rangle');
hold on;

% m_om = zeros(size(j_a))
%     z2=exp((alpha/(beta*D))*((ja-h-beta*cos(x)).*(ja-h-beta*cos(x))));