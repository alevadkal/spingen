function y = f3(x)
global alfa;
global k;
global beta;
global h;
global D;
y = k*x^3+(-beta/alfa+h)*x*x+(-k+D*(0.5/alfa))*x-h+beta/alfa;
end