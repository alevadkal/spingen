#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <string>
#include <cmath>
#include <iomanip>
#include <random>
#include "common.h"

void usage()
{
    std::string strings[]=
    {
        "Нормальное распределение",
        "-?                     Показать этот текст",
        "",
        "-m<value>",
        "--mean<value>          Среднее значение. По умолчанию 0",
        "",
        "-d<value>",
        "--deviation<value>     нормальное отклонение случайно последовательности. По умолчанию 0.1",
        "",
        "-s<value>",
        "--seed<value>          зерно случайной последовательности. По умолчанию 0",
        "",
        "-l<value>",
        "--length<value>        длинна последовательности. Если не задано - генерация идёт бесконечно",
    };
    for(auto string:strings) std::cerr<<string<<std::endl;
}
int main(int argc, char *argv[])
{
    bool debug=false;
    double mean=0; //Медиана случаного распределения.
    double deviation=0.1; //Нормальное отклонение случанойго распределения.
    unsigned long long seed = 0;
    unsigned long long length = 0;  //Длинна последовательности
    bool isLengthSet = false;       //Устанавливается в истину если длинна была передана как параметр
    //-----------------------------------------------------------------------------------------------------------
    //Парсим входные параметры.
    //-----------------------------------------------------------------------------------------------------------
    int option_index=0;
    int option_index_long=0;
    static struct option long_options[] =
    {
        {"mean",      required_argument, 0,  'm'},
        {"deviation", required_argument, 0,  'd'},
        {"seed",      required_argument, 0,  's'},
        {"length",    required_argument, 0,  'l'},
        {"debug",     no_argument,       0,   0 },
        {0,           0,                 0,   0 }
    };
    while ((option_index = getopt_long(argc, argv, "s:m:d:", long_options, &option_index_long)) != -1)
    {
        switch (option_index)
        {
        case 0:
            if(std::string(long_options[option_index_long].name)=="debug")
            {
                debug=true;
            }
            break;
        case '?':
            usage();
            return 0;
        case 'm':
            mean = stringToValue(optarg);
            break;
        case 'd':
            deviation = stringToValue(optarg);
            break;
        case 's':
            seed = (unsigned long long)stringToValue(optarg);
            break;
        case 'l':
            length = (unsigned long long)stringToValue(optarg);
            isLengthSet = true;
            break;
        default:
            std::cerr << "Unknown option:" << option_index << std::endl;
            usage();
            return 1;
        };
    };
    if(debug)
    {
        std::cerr << "seed      is " << seed << std::endl;
        std::cerr << "mean      is " << mean << std::endl;
        std::cerr << "deviation is " << deviation << std::endl;
    }
    //-----------------------------------------------------------------------------------------------------------
    //Основная часть программы.
    //-----------------------------------------------------------------------------------------------------------
    std::mt19937_64 generator(seed);
    std::normal_distribution<double> distribution(mean,deviation);
    std::cout << std::setprecision(20);
    while(!isLengthSet || length--)
    {
        std::cout << distribution(generator) << std::endl;
    }
    return 0;
}
