#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
#include <math.h>
#include <cstring>
#define IM1 2147483563
#define IM2 2147483399
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define EPS 1.2e-7
#define RNMX (1.0-EPS)
#define pi 3.1415926535897
#define dpi 2*3.1415926535897

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)
#include<stdlib.h>
//#include<conio.h>
#include<stdio.h>
#include<math.h>
#include <sstream>
#include <unistd.h>


//*********************************
double ran1(long *idum)

{
    static long iy=0;
    static long iv[NTAB];
    int j;
    long k;
    double temp;

    if (*idum<=0||!iy)
    {
        if(-(*idum)<1) *idum=1;
        else *idum=-(*idum);
        for (j=NTAB+7; j>=0; j--)
        {
            k=(*idum)/IQ;
            *idum=IA*(*idum-k*IQ)-IR*k;
            if (*idum<0) *idum+=IM;
            if(j<NTAB) iv[j]=*idum;
        }
        iy=iv[0];
    }
    k=(*idum)/IQ;
    *idum=IA*(*idum-k*IQ)-IR*k;
    if (*idum<0) *idum+=IM;
    j=iy/NDIV;
    iy=iv[j];
    iv[j]=*idum;
    if ((temp=AM*iy)>RNMX) return RNMX;
    else return temp;
}
//**********************

double gasdev(long *idum)
{
    static int iset=0;
    double ran1(long *idum);
    static double gset;
    double fac,rsq,v1,v2;

    if(*idum<0) iset=0;
    if(iset==0)
    {
        do
        {
            v1=2.0*ran1(idum)-1.0;
            v2=2.0*ran1(idum)-1.0;
            rsq=v1*v1+v2*v2;
        }
        while (rsq>=1.0||rsq==0.0);
        fac=sqrt(-2.0*log(rsq)/rsq);
        gset=v1*fac;
        iset=1;
        return v2*fac;
    }
    else
    {
        iset=0;
        return gset;
    }
}
std::string getDatFileName(double Dh)
{
    std::stringstream ss;
    ss<<"t_th_ph_"<<Dh<<".dat";
    return ss.str();
}
std::string getCumulantFileName(double Dh)
{
    std::stringstream ss;
    ss<<"cumulantsn_"<<Dh<<".dat";
    return ss.str();
}
std::string getSourceFileName(double Dh,double Jalpha)
{
    std::stringstream ss;
    ss<<"sorce_"<<Dh<<"_"<<Jalpha<<".dat";
    return ss.str();
}

int main(int argc, const char** argv)
{
    if(argc!=2) return -1;
    double DH = strtod(argv[1],NULL);    // Интенсивность шума внешнего магнитного поля
    printf("Calculate for %f\n",DH);

    std::string dataFileName=getDatFileName(DH);
    std::string cumulantFileName=getCumulantFileName(DH);

    int i;                    // Индекс
    double t;                 // Время
    double t0 = 400.;         // Время установления процесса
    double T1 = 200.;         // Шаг выборок после установления процесса
    double a = 0.02;          // Параметр α
    double C = 10;
    double ja;                // Параметр (j/ α)
    double hz = 1.2;          // Внешнее магнитное поле
    double b = -1.;           // Параметр β
    const int M = 10000;
    double t_end = t0 + M*T1; // Конечное время
    double h = 0.02;          // Шаг интегрирования
    double y1;                // Угол θ
    double y2;                // Угол φ
    double y3;                // Шумовые слагаемые
    double y4;
    double y5;
    double k1, k2, k3, k4;    // Коэффициенты Рунге-Кутты
    double l1, l2, l3, l4;
    double m1, m2, m3, m4;
    double n1, n2, n3, n4;
    double p1, p2, p3, p4;
    double phi_0;             // Начальный угол φ установившегося проццесса
    double theta_sqr_avr;     // Среднее квадрата θ
    double D_om;              // Дисперсия частоты
    double D_th;              // Дисперсия угла θ
    double delta_sqr_avr;     // Промежуточные переменные, используемые при
    double phi;               // расчете дисперсий частоты и угла θ
    double theta_avr;         // Среднее значение угла θ
    double dWin1, dWin2, dWin3;// Приращения виннеровского процесса
    double term1, term2, term3;
    double sumg1=0., sumg2=0., sumg3=0.;
    double mnogo=0., mgg;
    double nstrm1;      // Шумовое слагаемое 1-го уравнения
    double nstrm2;            // Шумовое слагаемое 2-го уравнения
    double nstrm_avr1;      // Среднее шумового слагаемого 1-го уравнения
    double nstrm_avr2;      // Среднее шумового слагаемого 2-го уравнения
    double nstrm_sqr_avr1; // Среднее квадрата шумового слагаемого 1-го ур
    double nstrm_sqr_avr2; // Среднее квадрата шумового слагаемого 2-го ур
    double D_nstrm1;      // Дисперсия шумового слагаемого 1-го ур
    double D_nstrm2;      // Дисперсия шумового слагаемого 2-го ур
    double omega_avr;     // Среднее значение частоты генерации

    double dH = sqrt(DH);
    double s = sqrt(h);
    long idum;
    idum = -1;

    FILE *fc;
    fc = fopen (cumulantFileName.c_str(),"a"); // Вывод результатов в виде файла
    fprintf (fc, "\nt0 = %f, t_end = %f, T1 = %f, M = %d, a = %f, b = %f, h = %f, DH = %f", t0, t_end, T1, M, a, b, h, DH);
    fprintf (fc, "\n ja      omega_avr  D_om  theta_avr D_th nstrm_avr1  D_nstrm1 nstrm_avr2  D_nstrm2\n");
    fclose (fc);

    y1 = 0.405064;     // Начальное значение угла θ
    y2 = 1.49881;      // Начальное значение угла φ
    y3 = 0.000;
    y4 = 0.000;
    y5 = 0.000;
    printf("%f:Heating gasdev!\n", DH);
    for (mgg = 0; mgg < 1E8; mgg += 1.)
    {
        mnogo = mnogo + 1.;
        term1 = gasdev(&idum);
        sumg1 += term1;
        term2 = gasdev(&idum);
        sumg2 += term2;
        term3 = gasdev(&idum);
        sumg3 += term3;
    }
    for (ja = -0.5; ja < 2.9; ja += 0.1)
    {
        printf("%f:Calculating for Jalpha=%f\n",DH,ja);
        y1 = 0.405064;
        y2 = 1.49881;
        y3 = 0.000;
        y4 = 0.000;
        y5 = 0.000;
        D_om = 0.;
        D_th = 0.;
        theta_avr = 0.;
        delta_sqr_avr = 0.;
        nstrm_sqr_avr1 = 0.;
        nstrm_sqr_avr2 = 0.;
        nstrm_avr1 = 0.;
        nstrm_avr2 = 0.;
        theta_sqr_avr = 0.;
        printf("%f:Wait while spingen stabilised!",DH);
        for (t = 0.; t <= t0; t+=h)
        {
            // Решение диф ур
            mnogo = mnogo + 1.;        // (процесс установления)
            term1 = gasdev(&idum);
            sumg1 += term1;
            dWin1 = s*dH*(term1 - sumg1/mnogo);
            term2 = gasdev(&idum);
            sumg2 += term2;
            dWin2 = s*dH*(term2 - sumg2/mnogo);
            term3 = gasdev(&idum);
            sumg3 += term3;
            dWin3 = s*dH*(term3 - sumg3/mnogo);

            m1 = h*(-C*y3) - C*dWin1;
            n1 = h*(-C*y4) - C*dWin2;
            p1 = h*(-C*y5) - C*dWin3;

            k1 = h*a*sin(y1)*(ja - (hz + b*cos(y1))) - h*sin(y2)*y3 + h*cos(y2)*y4;

            l1 = h*(hz + b*cos(y1))
                 - h*cos(y2)*(cos(y1)/sin(y1))*y3
                 - h*sin(y2)*(cos(y1)/sin(y1))*y4 + h*y5;

            m2 = h*(-C*(y3 + m1/2)) - C*dWin1;
            n2 = h*(-C*(y4 + n1/2)) - C*dWin2;
            p2 = h*(-C*(y5 + p1/2)) - C*dWin3;

            k2 = h*a*sin(y1 + k1/2)*(ja - (hz + b*cos(y1 + k1/2))) - h*sin(y2 + l1/2)*(y3 + m1/2) + h*cos(y2 + l1/2)*(y4 + n1/2);

            l2 = h*(hz + b*cos(y1 + k1/2))
                 - h*cos(y2 + l1/2)*(cos(y1 + k1/2)/sin(y1 + l1/2))*(y3 + m1/2)
                 - h*sin(y2 + l1/2)*(cos(y1 + k1/2)/sin(y1 + l1/2))*(y4 + n1/2) + h*(y5 + p1/2);

            m3 = h*(-C*(y3 + m2/2)) - C*dWin1;
            n3 = h*(-C*(y4 + n2/2)) - C*dWin2;
            p3 = h*(-C*(y5 + p2/2)) - C*dWin3;

            k3 = h*a*sin(y1 + k2/2)*(ja - (hz + b*cos(y1 + k2/2))) - h*sin(y2 + l2/2)*(y3 + m2/2) + h*cos(y2 + l2/2)*(y4 + n2/2);

            l3 = h*(hz + b*cos(y1 + k2/2))
                 - h*cos(y2 + l2/2)*(cos(y1 + k2/2)/sin(y1 + l2/2))*(y3 + m2/2)
                 - h*sin(y2 + l2/2)*(cos(y1 + k2/2)/sin(y1 + l2/2))*(y4 + n2/2) + h*(y5 + p2/2);

            m4 = h*(-C*(y3 + m3)) - C*dWin1;
            n4 = h*(-C*(y4 + n3)) - C*dWin2;
            p4 = h*(-C*(y5 + p3)) - C*dWin3;

            k4 = h*a*sin(y1 + k3)*(ja - (hz + b*cos(y1 + k3))) - h*sin(y2 + l3)*(y3 + m3) + h*cos(y2 + l3)*(y4 + n3);

            l4 = h*(hz + b*cos(y1 + k3))
                 - h*cos(y2 + l3)*(cos(y1 + k3)/sin(y1 + l3))*(y3 + m3)
                 - h*sin(y2 + l3)*(cos(y1 + k3)/sin(y1 + l3))*(y4 + n3) + h*(y5 + p3);

            y3 += (m1 + 2*m2 + 2*m3 + m4)/6;
            y4 += (n1 + 2*n2 + 2*n3 + n4)/6;
            y5 += (p1 + 2*p2 + 2*p3 + p4)/6;

            y1 += (k1 + 2*k2 + 2*k3 + k4)/6;
            y2 += (l1 + 2*l2 + 2*l3 + l4)/6;
        }

        printf("%f:Calculating...",DH);

        FILE *fb;

        fb = fopen (dataFileName.c_str(),"a");
        //fprintf (fb, "%f   %f   %f\n", t, y1, y2);
        phi_0 = y2;
        phi = y2;
        for (i = 0, t = t0; t <= t_end; i++, t += h)
        {

            // Решение диф ур
            mnogo = mnogo + 1.;      // (установившийся процесс)
            term1 = gasdev(&idum);
            sumg1 += term1;
            dWin1 = s*dH*(term1 - sumg1/mnogo);

            term2 = gasdev(&idum);
            sumg2 += term2;
            dWin2 = s*dH*(term2 - sumg2/mnogo);

            term3 = gasdev(&idum);
            sumg3 += term3;
            dWin3 = s*dH*(term3 - sumg3/mnogo);

            m1 = h*(-C*y3) - C*dWin1;
            n1 = h*(-C*y4) - C*dWin2;
            p1 = h*(-C*y5) - C*dWin3;

            k1 = h*a*sin(y1)*(ja - (hz + b*cos(y1))) - h*sin(y2)*y3 + h*cos(y2)*y4;

            l1 = h*(hz + b*cos(y1))
                 - h*cos(y2)*(cos(y1)/sin(y1))*y3
                 - h*sin(y2)*(cos(y1)/sin(y1))*y4 + h*y5;

            m2 = h*(-C*(y3 + m1/2)) - C*dWin1;
            n2 = h*(-C*(y4 + n1/2)) - C*dWin2;
            p2 = h*(-C*(y5 + p1/2)) - C*dWin3;

            k2 = h*a*sin(y1 + k1/2)*(ja - (hz + b*cos(y1 + k1/2))) - h*sin(y2 + l1/2)*(y3 + m1/2) + h*cos(y2 + l1/2)*(y4 + n1/2);

            l2 = h*(hz + b*cos(y1 + k1/2))
                 - h*cos(y2 + l1/2)*(cos(y1 + k1/2)/sin(y1 + l1/2))*(y3 + m1/2)
                 - h*sin(y2 + l1/2)*(cos(y1 + k1/2)/sin(y1 + l1/2))*(y4 + n1/2) + h*(y5 + p1/2);

            m3 = h*(-C*(y3 + m2/2)) - C*dWin1;
            n3 = h*(-C*(y4 + n2/2)) - C*dWin2;
            p3 = h*(-C*(y5 + p2/2)) - C*dWin3;

            k3 = h*a*sin(y1 + k2/2)*(ja - (hz + b*cos(y1 + k2/2))) - h*sin(y2 + l2/2)*(y3 + m2/2) + h*cos(y2 + l2/2)*(y4 + n2/2);

            l3 = h*(hz + b*cos(y1 + k2/2))
                 - h*cos(y2 + l2/2)*(cos(y1 + k2/2)/sin(y1 + l2/2))*(y3 + m2/2)
                 - h*sin(y2 + l2/2)*(cos(y1 + k2/2)/sin(y1 + l2/2))*(y4 + n2/2) + h*(y5 + p2/2);

            m4 = h*(-C*(y3 + m3)) - C*dWin1;
            n4 = h*(-C*(y4 + n3)) - C*dWin2;
            p4 = h*(-C*(y5 + p3)) - C*dWin3;

            k4 = h*a*sin(y1 + k3)*(ja - (hz + b*cos(y1 + k3))) - h*sin(y2 + l3)*(y3 + m3) + h*cos(y2 + l3)*(y4 + n3);

            l4 = h*(hz + b*cos(y1 + k3))
                 - h*cos(y2 + l3)*(cos(y1 + k3)/sin(y1 + l3))*(y3 + m3)
                 - h*sin(y2 + l3)*(cos(y1 + k3)/sin(y1 + l3))*(y4 + n3) + h*(y5 + p3);

            y3 += (m1 + 2*m2 + 2*m3 + m4)/6;
            y4 += (n1 + 2*n2 + 2*n3 + n4)/6;
            y5 += (p1 + 2*p2 + 2*p3 + p4)/6;

            y1 += (k1 + 2*k2 + 2*k3 + k4)/6;
            y2 += (l1 + 2*l2 + 2*l3 + l4)/6;
        }
        fprintf (fb, "\nt0 = %f, t_end = %f, T1 = %f, M = %d, a = %f, ja = %f, b = %f, h = %f, DH = %f", t0, t_end, T1, M, a, ja, b, h, DH);
        fclose (fb);
        D_nstrm1 = nstrm_sqr_avr1 - (nstrm_avr1*nstrm_avr1);

        D_nstrm2 = nstrm_sqr_avr2 - (nstrm_avr2*nstrm_avr2);

        D_th = theta_sqr_avr - (theta_avr*theta_avr);
        omega_avr = (y2 - phi_0)/(t_end - t0);
        D_om = delta_sqr_avr - (omega_avr*omega_avr);
        FILE *fc;
        fc = fopen (cumulantFileName.c_str(),"a");
        fprintf (fc, "%f %f %f %f %f %f %f %f %f\n\n", ja, omega_avr, D_om, theta_avr, D_th, nstrm_avr1, D_nstrm1, nstrm_avr2, D_nstrm2);
        fclose (fc);
    }
    return 0;
}
void usage()
{
    std::string strings[]=
    {
        "Спиновый генератор в декартовой системе координат.",
        "",
        "   на вход принимает случайный шум. На основе этого шума формируются трёхмерные вектора шума",
        "   т.е. на каждую итарацию берётся три значения из входного потока",
        "Принимаемые параметры:",
        "",
        "-?                    напечатать этот текст",
        "",
        "--lambda<value>       Параметр затухания гильберта",
        "",
        "--J<value>            Значение характеризующее величину тока.",
        ""
        "--k_eff<value>        параметр, характеризующий поле анизотропии ферромагнетика",
        "",
        "--h<theta,psi[,r]>    внешнее магнитное поле",
        "",
        "--start<theta,psi>    Стартовое положение вектора вектора намагниченности спиновго генератора",
        "",
        "--dt<value>           Шаг по оси dt",
        "",
        "--sphere_axis         Вывести данные в сферических координатах",
        "",
        "--devider             Колличество значений пропускаемых перед выводом",
        "",
        "--debug               Вывести данные c которыми запущена программа перед стартом расчёта",
    };
    for(auto string:strings) std::cerr<<string<<std::endl;
}
