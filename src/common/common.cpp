#include <string>
#include <cstdlib>
#include <valarray>

///Get double from string
double stringToValue(const char* str)
{
    return strtod(str,NULL);
}

///Get double from string
double stringToValue(std::string& str)
{
    return stringToValue(str.c_str());
}

///Replace symbol in string
std::string replaceSymbol(std::string& src,char from, char to)
{
    std::string out;
    for(auto c:src)
    {
        if(c == from) c=to;
        out=out+c;
    }
    return out;
}
///Get double array from string as "val,vak,val"
std::valarray<double> getVectFromStr(std::string& str)
{
    std::string value_spaces = replaceSymbol(str, ',', ' ');
    char* end;
    double vector_data[3]=
    {
        strtod(value_spaces.c_str(),&end),
        ((*end)?strtod(end,&end):0.),
        ((*end)?strtod(end,NULL):1.)
    };
    return std::valarray<double>(vector_data,3);
}
std::valarray<double> getVectFromStr(const char* str)
{
    std::string value(str);
    return getVectFromStr(value);
}


