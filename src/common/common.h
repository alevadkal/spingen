#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED
#include <string>
#include <valarray>

///Get double from string
double stringToValue(std::string& str);
double stringToValue(const char* str);

///Get double array from string as "val,vak,val"
std::valarray<double> getVectFromStr(std::string& str);
std::valarray<double> getVectFromStr(const char* str);


#endif // COMMON_H_INCLUDED
