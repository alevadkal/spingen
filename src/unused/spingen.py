#! /bin/python
#coding=utf8
import math
import numpy as np
from numpy import cross
import random
import math

def cart2pol(x, y, z):
    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arccos(z/r)
    phi=np.arctg(y/x)
    return  r, theta, phi

def pol2cart(r, theta, phi):
    x = r * np.sin(theta) * np.cos(phi),
    y = r * np.sin(theta) * np.sin(phi),
    z = r * np.cos(phi)
    return x, y, z

def abs_vec(vec):
    sqrt((right*right).sum())
def scalar(left,right):
    return (left * right).sum()


class rungeStoch:
    
    def __init__(self,dt=0.00001):
        self.dt=dt
        self.ds=math.sqrt(dt)
    
    def step(self, stochDifur, input, t):
        new_t = t + self.dt
        k1 = stochDifur.determ(input, new_t) * self.dt + stochDifur.stoch(input, new_t) * self.ds
        k2 = stochDifur.determ(input + k1/2., new_t) * self.dt + stochDifur.stoch(input + k1/2, new_t) * self.ds
        k3 = stochDifur.determ(input + k2/2., new_t) * self.dt + stochDifur.stoch(input + k2/2, new_t) * self.ds
        k4 = stochDifur.determ(input + k3, new_t) * self.dt + stochDifur.stoch(input + k3, new_t) * self.ds
        return input + (k1 + k2 *2 + k3 *2 +k4)/6., new_t

class Spingen:
    def  __init__(
        self,
        lambda_val,
        J,
        k_eff,
        h_r,
        h_theta,
        h_phi
    ):
        self.lambda_val =lambda_val
        self.J = J
        self.k_eff = k_eff
        self.h=np.array([h_r, h_theta, h_phi])
        self.e_s=np.array([1, h_theta, h_phi])
        self.random=np.array([0, 0, 0])
    
    def determ(self, input, t=0):
        return input, t
    
    def stoch(self, input, t=0):
        return input, t
    
    def setNoise(self, random):
        self.random=random


class spingenDescartes(Spingen):
    def __init__(self,
        lambda_val,
        J,
        k_eff,
        h_r,
        h_theta,
        h_phi
    ):
        Spingen.__init__(self,lambda_val, J, k_eff, h_r, h_theta, h_phi)
        self._updateH()
        self._updateE_S()
    
    def _updateH(self):
        self.h=np.array([pol2cart(self.h[0],self.h[1],self.h[2])])
    def _updateE_S(self):
        self.e_s=np.array([pol2cart(self.e_s[0],self.e_s[1],self.e_s[2])])

    def determ(self,input, t=0):
        abs_input=abs_vec(input)
        h_eff = sel.k_eff * input * scalar(input, self.h) / abs_input
        return (
            cross(h_eff, input) -
            self.lambda_val * cross(input, cross(input, h_eff)) +
            self.J * cross(input, cross(input, input* scalar(input ,self.e_s)/ abs_input))
        )

    def stoch(self, input, t=0):
        return cross(self.random, input)

class RandNormal:
    def __init__(self, mean, std, seed=666):
        random.seed(seed)
        self._state=random.get_state()
        self.mean=mean
        self.std=std

    def getValue():
        random.set_state(self._state)
        value=random.gauss(self.mean,self.std)
        self._state=random,get_state()
        return value;


class FreqCalculator:
    def __init__(
        self,
        Jalpha,
        deviation,
        length,
        passCount,
        cutFirst,
        start_theta = 0,
        start_phi = 0,
        t=0,
        lambda_val = 0.02,
        k_eff = -1,
        h_r = 1.2,
        h_theta = 0,
        h_phi = 0,
        dt = 0.00001,
    ):
        self.input = self._getStart(start_theta,start_phi)
        self.t = t
        self.length = length
        self.passCount = passCount
        self.cutFirst = cutFirst
        self.deviation = deviation
        self.random = RandNormal(0,deviation)
        self.J = Jalpha/lambda_val
        self.spingen = spingenDescartes(lambda_val, self.J, k_eff, h_r, h_theta, h_phi);
        self.integrator=rungeStoch(dt)

    def _getStart(self, theta, phi):
        return pol2cart(1,np.array([theta]),np.array(phi))

    def iteration(self):
        self.spingen.setNoise(np.array([self.random.getValue() for i in range(3)]))
        self.input, self.t = self.integrator.step(self.spingen,self.input,self.t)

    def getFreqStd(self):
        for i in range(int(self.cutFirst * self.passCount)):
            self.iteration()
        
        data=np.empty(self.length)
        for i in range(self.length):
            data[i]=np.array(
                [self.iteration[0] for j in range(self.passCount)]
            ).mean()

        return np.std(np.fft(data))

def main():
    dt=0.001
    passCount=200
    T0=100
    T1=10000
    
    start_theta = 2
    start_phi = 2
    
    length = T1 * passCount / dt
    cutFirst = T0 / dt

    deviations=[val**-10 for val in range(1,4)]
    Jalphas=[val/10 for val in range(-5, 30)]
    data={}
    for deviation in deviations:
        deviationData=np.empty(len(Jalphas))
        count=0
        for Jalpha in Jalphas:
            print "calc for {deviation} {Jalpha} start".format(
                deviation=deviation,
                Jalpha=Jalpha
            )
            calc=FreqCalculator(Jalpha,deviation,length,passCount,cutFirst)
            deviationData[count]=calc.getFreqStd()
            count += 1
            print "calc for {deviation} {Jalpha} done: {val}".format(
                deviation=deviation,
                Jalpha=Jalpha,
                val=deviationData[count]
            )
        data.add(str(deviation),deviationData)

if __name__ == "__main__":
    main()