from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import visvis as vv
import sys

xColumn=1
yColumn=2
zColumn=3

data=[]

count=0

for line in sys.stdin:
  count=count+1
  lineData=line.strip()
  data.append(lineData)

npData=np.array(data)

plt.plot(npData)
plt.show()