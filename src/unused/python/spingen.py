#! /bin/python
#coding=utf8
#Как результат сама мат модель получилась очень медленной. Даже на моём железе на 10000 итераций уходит около секунды.

import math
import sys
import numpy as np
from numpy import cross
import random
import math
import logging

logger = logging.getLogger('spingen')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

def cart2pol(x, y, z):
    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arccos(z/r)
    phi=np.arctg(y/x)
    return  r, theta, phi

def pol2cart(r, theta, phi):
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(phi)
    return x, y, z

def abs_vec(vec):
    return math.sqrt((vec*vec).sum())

def scalar(left,right):
    return (left * right).sum()


class rungeStoch:
    
    def __init__(self,dt=0.00001):
        self.dt=dt
        self.ds=math.sqrt(dt)
        logger.debug("%s dt:%f, ds:%f",self.__class__, self.dt, self.ds)
    
    def step(self, stochDifur, input, t):
        new_t = t + self.dt
        k1 = stochDifur.determ(input, new_t) * self.dt + stochDifur.stoch(input, new_t) * self.ds
        k2 = stochDifur.determ(input + k1/2., new_t) * self.dt + stochDifur.stoch(input + k1/2, new_t) * self.ds
        k3 = stochDifur.determ(input + k2/2., new_t) * self.dt + stochDifur.stoch(input + k2/2, new_t) * self.ds
        k4 = stochDifur.determ(input + k3, new_t) * self.dt + stochDifur.stoch(input + k3, new_t) * self.ds
        return input + (k1 + k2 *2 + k3 *2 +k4)/6., new_t

class Spingen:
    def  __init__(
        self,
        lambda_val,
        J,
        k_eff,
        h_r,
        h_theta,
        h_phi
    ):
        self.lambda_val =lambda_val
        self.J = J
        self.k_eff = k_eff
        self.h=np.array([h_r, h_theta, h_phi])
        self.e_s=np.array([1, h_theta, h_phi])
        self.random=np.array([0, 0, 0])
    
    def determ(self, input, t=0):
        return input, t
    
    def stoch(self, input, t=0):
        return input, t
    
    def setNoise(self, random):
        self.random=random


class SpingenDescartes(Spingen):
    def __init__(self,
        lambda_val,
        J,
        k_eff,
        h_r,
        h_theta,
        h_phi
    ):
        Spingen.__init__(self,lambda_val, J, k_eff, h_r, h_theta, h_phi)
        logger.debug("construct %s",self.__class__)
        self._updateH()
        self._updateE_S()
        logger.debug("lambda: %f, J:%f, k_eff:%f",lambda_val,J,k_eff);
        logger.debug("h: %s",str(self.h))
        logger.debug("e_s: %s",str(self.e_s))
    
    def _updateH(self):
        x,y,z=pol2cart(self.h[0],self.h[1],self.h[2])
        self.h=np.array([x,y,z])
    def _updateE_S(self):
        x,y,z=pol2cart(self.e_s[0],self.e_s[1],self.e_s[2])
        self.e_s=np.array([x,y,z])

    def determ(self,input, t=0):
        abs_input=abs_vec(input)
        h_eff = self.k_eff * input * scalar(input, self.h) / abs_input
        return (
            cross(h_eff, input) -
            self.lambda_val * cross(input, cross(input, h_eff)) +
            self.J * cross(input, cross(input, input* scalar(input ,self.e_s)/ abs_input))
        )

    def stoch(self, input, t=0):
        return cross(self.random, input)

class FreqCalculator:
    def __init__(
        self,
        Jalpha,
        deviation,
        length,
        passCount,
        cutFirst,
        start_theta = 0,
        start_phi = 0,
        t=0,
        lambda_val = 0.02,
        k_eff = -1,
        h_r = 1.2,
        h_theta = 0,
        h_phi = 0,
        dt = 0.00001,
        seed=666,
    ):
        self.input = self._getStart(start_theta,start_phi)
        self.t = t
        self.length = length
        self.passCount = passCount
        self.cutFirst = cutFirst
        self.deviation = deviation
        self.random = random.WichmannHill(seed)
        self.J = Jalpha/lambda_val
        self.spingen = SpingenDescartes(lambda_val, self.J, k_eff, h_r, h_theta, h_phi);
        self.integrator=rungeStoch(dt)
        logger.debug("construct %s",self.__class__)

    def _getStart(self, theta, phi):
        return np.array(pol2cart(1,theta,phi))

    def _iteration(self):
        self.spingen.setNoise(np.array([self.random.gauss(0, self.deviation) for i in range(3)]))
        self.input, self.t = self.integrator.step(self.spingen,self.input,self.t)

    def getFreqStd(self):
        cuttingFirst=int(self.cutFirst * self.passCount)
        logger.debug("cutting first %i", cuttingFirst)
        cuttingFirst_01=cuttingFirst/100
        for i in xrange(cuttingFirst):
            print str(float(i)/cuttingFirst_01)+"%\r",
            self._iteration()
        print
        logger.debug("calculation start!")
        data=np.empty(self.length)
        for i in range(self.length):
            data[i]=np.array(
                [self._iteration[0] for j in range(self.passCount)]
            ).mean()
        logger.debug("calculation done!")
        logger.debug("get fft!")
        
        return np.std(np.fft(data))

def main():
    dt=0.01
    passCount=200
    T0=100
    T1=10000
    
    start_theta = 2
    start_phi = 2
    
    length = T1 * passCount / dt
    cutFirst = T0 / dt
    logger.debug("Length for each calc:%i; cut first:%i", length,cutFirst)

    deviations=[10**-val for val in range(1,4)]
    Jalphas=[val/10 for val in range(-5, 30)]
    data={}
    for deviation in deviations:
        deviationData=np.empty(len(Jalphas))
        count=0
        logger.info("=====================================================================")
        logger.info("calc for deviation %f",deviation)
        logger.info("=====================================================================")
        
        for Jalpha in Jalphas:
            logger.info("calc for %f", Jalpha)
            calc=FreqCalculator(Jalpha,deviation,length,passCount,cutFirst)
            deviationData[count]=calc.getFreqStd()
            count += 1
        data.add(str(deviation),deviationData)

if __name__ == "__main__":
    main()