/**
 * @file
 * @author     Alexander Kalyuzhnyy
 * @date       2017
*/
#include "SpinGeneratorDescartesAxis.hpp"
#include "VectorUtils.hpp"
Vector cross(const Vector& l, const Vector &r)
{
    assert(l.size()==r.size());
    assert(l.size()==3);

    Vector ret(3);
    ret[0] = l[1] * r[2] - l[2] * r[1];
    ret[1] = l[2] * r[0] - l[0] * r[2];
    ret[2] = l[0] * r[1] - l[1] * r[0];
    return ret;
}


SpinGeneratorDescartesAxis::SpinGeneratorDescartesAxis( const double& lambdaVal, const double& JVal, const double& k_effVal, const std::valarray<double>& hVal):
    SpinGenerator(lambdaVal,JVal,k_effVal,hVal) {}

std::valarray<double> SpinGeneratorDescartesAxis::determinateUnit(const std::valarray<double>& in, const double& t) const
{
    //Дифференциальное уроавение не зависит явно от времени
    (void)t;

    std::valarray<double> h_eff = h;
    std::valarray<double> e_s(3);

    //TODO Что-то я тут не догнал - надо будет проконсультироваться.
    e_s[2] = in[2];
    //Z-компонента магнитного момента нашего домена влияет на эффективную составляющую магнитного поля.
    h_eff[2] += k_eff * in[2];
    return cross(h_eff, in) - lambda * cross(in, cross(h_eff, in)) + J * cross(in, cross(in, e_s));
}
Vector SpinGeneratorDescartesAxis::stochasticUnit(const std::valarray<double>& in, std::function<double(void)> random, const Real& t) const
{
    double randomArray[]= {random(),random(),random()};
    std::valarray<double> randomVector= {randomArray,DIMENSION};
    //Дифференциальное уроавение не зависит явно от времени
    (void)t;
    //Направляем случайное воздействие по касательной к поверхности сферы.
    //т.к.in у нас -единичный вектор - это фактически выглядит как простая проекция вектора на плоскость касательную к поверхности сферы в данной точке.
    return cross(randomVector, in);
}

void SpinGeneratorDescartesAxis::stabilise(Vector& data) const
{
    //В сявзи с тем, что вычисления происходят с погрешностями - мы производим постоянную нормализацию получившейся величины к единичному вектору.
    data = normalyse(data);
}

