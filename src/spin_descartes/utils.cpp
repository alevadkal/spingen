#include "utils.hpp"
#include <cmath>
#include <cassert>
#ifndef M_PI
#define M_PI (3.14159265359)
#endif // M_PI

std::valarray<double> cross(const std::valarray<double>& left, const std::valarray<double> &right)
{
    assert(left.size()==3 && left.size()==right.size());

    return
    {
        left[1] * right[2] - left[2] * right[1],
        left[2] * right[0] - left[0] * right[2],
        left[0] * right[1] - left[1] * right[0],
    };
}
double scalar(const std::valarray<double>& left, const std::valarray<double> &right)
{
    return (left*right).sum();
}
std::valarray<double> toDescartes(const std::valarray<double>& obj)
{
    //obj[0] theta
    //obj[1] psi
    //obj[2] r
    return
    {
        obj[0] * sin(obj[1]) * cos(obj[2]),
        obj[0] * sin(obj[1]) * sin(obj[2]),
        obj[0] * cos(obj[2])
    };
}
std::valarray<double> toSphere(const std::valarray<double>& obj)
{
    //obj[0] theta  x
    //obj[1] psi    y
    //obj[2] r      z
    std::valarray<double> sq=obj*obj;
    return
    {
        sqrt(sq.sum()),
        atan(sqrt(sq[0]+sq[1])/obj[2]),
        atan(obj[1]/obj[0])
    };
}
double length(const std::valarray<double>& obj)
{
    return sqrt((obj*obj).sum());
}
std::valarray<double> normalyse(const std::valarray<double>& obj, const double& value)
{
    return (obj*value)/length(obj);
}
std::valarray<double> projectionAtoB(const std::valarray<double>& A, const std::valarray<double>& B)
{
    return B*scalar(A,B)/length(B);
}
