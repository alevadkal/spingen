#ifndef UTILS_HPP_INCLUDED
#define UTILS_HPP_INCLUDED
#include <valarray>

std::valarray<double> cross(const std::valarray<double>& left, const std::valarray<double> &right);
double scalar(const std::valarray<double>& left, const std::valarray<double> &right);

std::valarray<double> toDescartes(const std::valarray<double>& obj);
std::valarray<double> toSphere(const std::valarray<double>& obj);
double length(const std::valarray<double>& obj);
std::valarray<double> normalyse(const std::valarray<double>& obj, const double& value = 1.);
std::valarray<double> projectionAtoB(const std::valarray<double>& A, const std::valarray<double>& B);

#endif // UTILS_HPP_INCLUDED
