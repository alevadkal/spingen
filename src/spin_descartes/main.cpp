#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <string>
#include <random>
#include <stdexcept>
#include <valarray>
#include <functional>
#include <iomanip>
#include "SpinGeneratorDescartesAxis.hpp"
#include "RungeKutta4Stochastic.hpp"
#include "common.h"

#include "utils.hpp"

bool debug=false;


void usage()
{
    std::string strings[]=
    {
        "Спиновый генератор в декартовой системе координат.",
        "",
        "   на вход принимает случайный шум. На основе этого шума формируются трёхмерные вектора шума",
        "   т.е. на каждую итарацию берётся три значения из входного потока",
        "Принимаемые параметры:",
        "",
        "-?                    напечатать этот текст",
        "",
        "--lambda<value>       Параметр затухания гильберта",
        "",
        "--J<value>            Значение характеризующее величину тока.",
        ""
        "--k_eff<value>        параметр, характеризующий поле анизотропии ферромагнетика",
        "",
        "--h<theta,psi[,r]>    внешнее магнитное поле",
        "",
        "--start<theta,psi>    Стартовое положение вектора вектора намагниченности спиновго генератора",
        "",
        "--dt<value>           Шаг по оси dt",
        "",
        "--established_time    Время интегрирования перед началом вывода последовательности",
        "",
        "--integrate_time      Время интегриирования. Если не установлено - интегрирование продолжается",
        "                      пока есть последовательность чисел на входе"
        ",",
        "--devider             Колличество значений пропускаемых перед выводом",
        "",
        "--debug               Вывести данные c которыми запущена программа перед стартом расчёта",
    };
    for(auto string:strings) std::cerr<<string<<std::endl;
}

double getRandomFromStdin()
{
    double random;
    if(std::cin.eof())
    {
        std::cerr << "End of input flow! Exit!" << std::endl;
        exit(0);
    }
    std::cin >> random;
    return random;
}

struct SpinGeneratorData
{
    double lambda = 0.02;
    double J = 0.035;
    double k_eff = -1;
    std::valarray<double> h = {1.2, 0, 0};
    double establishedTime = 0;
    double integrateTime = 0;
    bool isIntegrateTimeSet = false;
    double dt = 0.001;
    double ds = sqrt(dt);
    std::valarray<double> start = {1, 2, 2};
    unsigned int devider = 1;
    std::function<double()> random = getRandomFromStdin;
};

void calculate(struct SpinGeneratorData& data)
{
    if(debug) std::cerr << "h value sphere: [" << data.h[0] << " " << data.h[1] << " " << data.h[2] << "]" << std::endl;
    data.h=toDescartes(data.h);
    if(debug) std::cerr << "h value descates: [" << data.h[0] << " " << data.h[1] << " " << data.h[2] << "]" << std::endl;
    if(debug) std::cerr << "start value sphere: [" << data.start[0] << " " << data.start[1] << " " << data.start[2] << "]" << std::endl;
    data.start=toDescartes(data.start);
    if(debug) std::cerr << "start value descartes: [" << data.start[0] << " " << data.start[1] << " " << data.start[2] << "]" << std::endl;

    SpinGeneratorDescartesAxis spinGenerator = {data.lambda, data.J, data.k_eff, data.h};

    double t = 0;
    double deviderCount = data.devider;
    std::valarray<double> value = data.start;

    //Ждём установки сигнала
    if(debug) std::cerr << "Start calculate for established values" << std::endl;
    while(t < data.establishedTime)
    {
        //if(debug) std::cerr<<t<<std::endl;
        spinGenerator.stabilise(value);
        std::valarray<double> random = {data.random(), data.random(), data.random()};
        rungeKutta4Stochastic(spinGenerator, value, random, t, data.dt, data.ds, value, t);
    }

    if(debug) std::cerr << "Start integration" << std::endl;
    //Интегрируем с выводом данных
    while(!data.isIntegrateTimeSet || t < data.integrateTime)
    {
        //if(debug) std::cerr<<t<<std::endl;
        spinGenerator.stabilise(value);
        if(data.devider == deviderCount++)
        {
            std::cout << value[0] << " " << value[1] << " " << value[2] << " " << t << std::endl;
            deviderCount = 1;
        }
        std::valarray<double> random = {data.random(),data.random(),data.random()};
        rungeKutta4Stochastic(spinGenerator, value, random, t, data.dt, data.ds, value, t);
    }
    if(debug) std::cerr << "Done integration" << std::endl;
}


int main(int argc, char *argv[])
{
    unsigned int PRECISION=18;
    std::cout<<std::setprecision(PRECISION);
    std::cerr<<std::setprecision(PRECISION);

    struct SpinGeneratorData data;

    int option_index=0;
    int option_index_long=0;

    static struct option long_options[] =
    {
        {"established_time", required_argument, 0, 0},
        {"integrate_time",   required_argument, 0, 0},
        {"lambda",           required_argument, 0, 0},
        {"J",                required_argument, 0, 0},
        {"k_eff",            required_argument, 0, 0},
        {"h",                required_argument, 0, 0},
        {"start",            required_argument, 0, 0},
        {"dt",               required_argument, 0, 0},
        {"ds",               required_argument, 0, 0},
        {"devider",          required_argument, 0, 0},
        {"debug",            no_argument,       0, 0},
        {"",                 no_argument,       0, 0},
        {0,                  0,                 0, 0}
    };

    while ((option_index = getopt_long(argc, argv, "s", long_options, &option_index_long)) != -1)
    {
        std::string option_string;
        switch (option_index)
        {
        case '?':
            usage();
            return 0;
        case 0:
            option_string = long_options[option_index_long].name;
            if(debug) std::cerr<<"parse index:"<< option_index_long << ", string:" << option_string << ", arg:" << optarg << std::endl;
            if(option_string == "debug")
            {
                debug=true;
            }
            else if(option_string == "established_time")
            {
                data.establishedTime = stringToValue(optarg);
            }
            else if(option_string == "integrate_time")
            {
                data.integrateTime = stringToValue(optarg);
                data.isIntegrateTimeSet = true;
            }
            else if(option_string == "lambda")
            {
                data.lambda = stringToValue(optarg);
            }
            else if(option_string == "J")
            {
                data.J = stringToValue(optarg);
            }
            else if(option_string == "k_eff")
            {
                data.k_eff = stringToValue(optarg);
            }
            else if(option_string == "dt")
            {
                data.dt = stringToValue(optarg);
                data.ds = sqrt(data.dt);
            }
            else if(option_string == "devider")
            {
                data.devider = (unsigned int)stringToValue(optarg);
            }
            else if(option_string == "h")
            {
                data.h = getVectFromStr(optarg);
            }
            else if(option_string == "start")
            {
                data.start=getVectFromStr(optarg);
            }
            else
            {
                throw std::runtime_error("Unknown parameter '" + option_string + "' with value '" + optarg + "'" );
            }
            break;
        default:
            std::cerr << "Unknown option:" << option_index << std::endl;
            usage();
            return 1;
        };
    };
    if(debug)
    {
        std::cerr<<"SUMMARY:" << std::endl;
        std::cerr<<"    lambda           is "<<data.lambda<<std::endl;
        std::cerr<<"    J                is "<<data.J<<std::endl;
        std::cerr<<"    k_eff            is "<<data.k_eff<<std::endl;
        std::cerr<<"    h                is ["<<data.h[0]<<","<<data.h[1]<<","<<data.h[2]<<"]"<<std::endl;
        std::cerr<<"    dt               is "<<data.dt<<std::endl;
        std::cerr<<"    ds               is "<<data.ds<<std::endl;
        std::cerr<<"    start            is ["<<data.start[0]<<","<<data.start[1]<<","<<data.start[2]<<"]"<<std::endl;
        std::cerr<<"    devider          is "<<data.devider<<std::endl;
        std::cerr<<"    established_time is "<<data.establishedTime<<std::endl;
        if(data.isIntegrateTimeSet == true)
        {
            std::cerr<<"    integrate_time   is "<<data.integrateTime<<std::endl;
        }

    }
    calculate(data);
}
