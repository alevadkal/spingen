/**
 * @file
 * @author     Alexander Kalyuzhnyy
 * @date       2017
*/
#include "SpinGeneratorDescartesAxis.hpp"
#include <valarray>
#include "../utils.hpp"

/*#include <sstream>
#include <iostream>

std::string showvector(const std::valarray<double> vect){
    std::stringstream ss;
    ss << "[" << vect[0] << " " << vect[1] << " " << vect[2]<<"]";
    return ss.str();
}*/

SpinGeneratorDescartesAxis::SpinGeneratorDescartesAxis( const double& lambdaVal, const double& JVal, const double& k_effVal, const std::valarray<double>& hVal):
    SpinGenerator(lambdaVal,JVal,k_effVal,hVal) {}

std::valarray<double> SpinGeneratorDescartesAxis::determinateUnit(const std::valarray<double>& in, const double& t) const
{
    //Дифференциальное уроавение не зависит явно от времени
    (void)t;


    std::valarray<double> h_eff = h + k_eff * projectionAtoB(in,h);
    return cross(h_eff, in) - lambda * cross(in, cross(in, h_eff)) + J * cross(in, cross(in, projectionAtoB(in,e_s)));
}
std::valarray<double> SpinGeneratorDescartesAxis::stochasticUnit(const std::valarray<double>& in, const std::valarray<double>& random, const double& t) const
{
    //Дифференциальное уроавение не зависит явно от времени
    (void)t;
    //Направляем случайное воздействие по касательной к поверхности сферы.
    //т.к.in у нас -единичный вектор - это фактически выглядит как простая проекция вектора на плоскость касательную к поверхности сферы в данной точке.
    return cross(random, in);
}

void SpinGeneratorDescartesAxis::stabilise(std::valarray<double>& data) const
{
    //В сявзи с тем, что вычисления происходят с погрешностями - мы производим постоянную нормализацию получившейся величины к единичному вектору.
    data = data/sqrt((data*data).sum());
}

