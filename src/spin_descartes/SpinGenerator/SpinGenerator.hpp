/// \file
/// \author     Alexander Kalyuzhnyy
/// \date       2016-2017

#ifndef SpinGenerator_HPP_INCLUDED
#define SpinGenerator_HPP_INCLUDED
#include "StochasticDiffEquation.hpp"
#include <valarray>

/**
 * @brief Интерфейс спинового генератора
 * Так же как и класс самого стохастического дифференциального уравнения является абстрактным
 * Определяет основные правила реализации спинового генератора.
 */
class SpinGenerator:public StochasticDiffEquation
{

public:

    SpinGenerator(const double& lambdaRef, const double& JRef, const double& k_effRef, const std::valarray<double>& hRef);

    ///Обработка погрешностей
    virtual void stabilise(std::valarray<double> &data) const  = 0;

protected:

    ///Параметры приведены в обозначениях принимаемых в литературе для большей удобочитаемости пользователем.

    ///Нормированный параметр магнитной релаксации Ландау-Лифшица.
    double lambda;
    ///Коэффициент характеризующий величину эффектов связанных с переносом спина током протекающим через структуру.
    double J;
    ///характеризует эффект анизотропии(включая анизотропию формы кристалла)
    double k_eff;
    ///Вектор внешнего магнитного поля.
    std::valarray<double> h;
    ///Направление намагниченности фиксированного слоя
    std::valarray<double> e_s;

};


#endif  //SPINGEN_HPP_INCLUDED
