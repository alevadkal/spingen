/// \file
/// \author     Alexander Kalyuzhnyy
/// \date       2017
#include "SpinGenerator.hpp"
#include "../utils.hpp"
SpinGenerator::SpinGenerator(const double& lambdaRef, const double& JRef, const double& k_effRef, const std::valarray<double>& hRef):
    lambda(lambdaRef),
    J(JRef),
    k_eff(k_effRef),
    h(hRef),
    e_s(normalyse(h))
{}

