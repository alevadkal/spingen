/**
 * @file
 * @author     Alexander Kalyuzhnyy
 * @date       2017
*/
#ifndef SpinGeneratorDescartesAxis_H_INCLUDED
#define SpinGeneratorDescartesAxis_H_INCLUDED
#include "SpinGenerator.hpp"

class SpinGeneratorDescartesAxis:public SpinGenerator
{
    static const int DIMENSION=3;
public:
    SpinGeneratorDescartesAxis( const double& lambdaVal, const double& JVal, const double& k_effVal, const std::valarray<double>& hVal);
    std::valarray<double> determinateUnit(const std::valarray<double>& in, const double& t) const;
    std::valarray<double> stochasticUnit(const std::valarray<double>& in, const std::valarray<double>& random, const double& t) const;
    void stabilise(std::valarray<double>& data) const override;
};
#endif // SPINGENDESCARTESAXIS_H_INCLUDED
