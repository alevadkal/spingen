/// \file
/// \author     Alexander Kalyuzhnyy
/// \date       2017
#include "StochasticDiffEquation.hpp"
#include <valarray>

void rungeKutta4Stochastic(
    const StochasticDiffEquation& stochasticDiffEquation,
    const std::valarray<double>& in,
    const std::valarray<double>& random,
    const double& t,
    const double& dt,
    const double& ds,
    std::valarray<double>& out,
    double& tOut
)
{
    std::valarray<double> k1 = stochasticDiffEquation.determinateUnit(in, t) * dt +
                               stochasticDiffEquation.stochasticUnit(in, random, t) * ds;

    std::valarray<double> k2 = stochasticDiffEquation.determinateUnit(in + k1 / (double)2., t) * dt +
                               stochasticDiffEquation.stochasticUnit(in+k1/ (double)2., random, t) * ds;

    std::valarray<double> k3 = stochasticDiffEquation.determinateUnit(in + k2 / (double)2., t) * dt +
                               stochasticDiffEquation.stochasticUnit(in+k2/(double)2., random, t) * ds;

    std::valarray<double> k4 = stochasticDiffEquation.determinateUnit(in + k3, t) * dt +
                               stochasticDiffEquation.stochasticUnit(in + k3, random, t) * ds;

    out = in + (k1 + k2 * (double)2. + k3 * (double)2. + k4 ) / (double)6.;
    tOut+=dt;
}
