/**
 * @file
 * @author     Alexander Kalyuzhnyy
 * @date       2017
*/
#ifndef StochasticDiffEquation_HPP_INCLUDED
#define StochasticDiffEquation_HPP_INCLUDED
#include <functional>
#include <valarray>

/// Интерфейс реализации дифференциального уравнения.
class StochasticDiffEquation
{
public:

    /// \brief Детерминированная часть стохастического дифференциального уравнения
    /// \param [in]  &in        Входное значение
    /// \param [in]  &t         Время
    /// \param [in]  &out       Выходное значение
    /// \return Возвращает ссылку на выходное значение
    virtual std::valarray<double> determinateUnit(const std::valarray<double>& in, const double& t) const=0;

    /// \brief Стохастическая часть стохастического дифференциального уравнения
    /// \param [in]  &in        Входное значение
    /// \param [in]  &random    Случайная величина
    /// \param [in]  &t         Время
    /// \param [in]  &out       Выходное значение
    /// \return Возвращает ссылку на выходное значение
    virtual std::valarray<double> stochasticUnit(const std::valarray<double>& in, const std::valarray<double>& random, const double& t) const =0;

    virtual ~StochasticDiffEquation() {}
};
#endif // STOCHDIFUR_HPP_INCLUDED
