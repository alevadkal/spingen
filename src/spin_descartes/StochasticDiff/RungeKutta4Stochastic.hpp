/// \file
/// \author     Alexander Kalyuzhnyy
/// \date       2015-2017

#ifndef RungeKutta4Stochastic_HPP_INCLUDED
#define RungeKutta4Stochastic_HPP_INCLUDED
#include "StochasticDiffEquation.hpp"
#include <functional>

/// \brief Реализация метода Рунге-Кутты 4-го порядка для стохастических уравнений
/// \param [in]  &stochDifur Стохастическое дифференциальное уравнение.
/// \param [in]  &in         Входное значение величины
/// \param [in]  &random     Величина случайного процесса воздействующего на систему
/// \param [in]  t           Время
/// \param [in]  dt          Шаг интегрирования по времени
/// \param [in]  ds          Среднеквадратичное значение шума.
/// \param [out] &out        Выходное значение параметров.
/// Функция построена так, что в качестве входных и выходных параметров может передаться ссылка на один и тот же объект
/// Инкрементация времени должна происходить за пределами интегратора.
/// Метод написан для интеграла Стратоновича и может отличаться от представленных в литературе т.к. там обычно даются примеры для интеграла Ито
void rungeKutta4Stochastic(
    const StochasticDiffEquation& stochasticDiffEquation,
    const std::valarray<double>& in,
    const std::valarray<double>& random,
    const double& t,
    const double& dt,
    const double& ds,
    std::valarray<double>& out,
    double& tOut
);
#endif //RUNGESTOCH_HPP_INCLUDED
