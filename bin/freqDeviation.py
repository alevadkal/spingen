#! /bin/python
#coding=utf8

#from spingen import SpingenDescartesExternal
#from randomGen import RandomNormalExternal
import numpy as np
import math
#import calculate

import logger, logging
from cmath import sqrt
logger = logger.logger('freq_deviation',logging.DEBUG)

class FreqParams:
    def __init__(self, result):
        """
            Класс расчитывающий частотные характеристики сигнала
        """
        length=len(result)
        logger.debug("data length:" + str(length))
        resultAutocorr = autocorr(result)
        freqPower = np.fft.fft(resultAutocorr)
        freqPower = np.absolute(freqPower)[0:(len(freqPower) / 2)]/(length*length)
        self._mean = (np.array(range(len(freqPower))) * freqPower).sum() / freqPower.sum()
        deviation = self._mean-np.array(range(len(freqPower)))
        self._std = (deviation * deviation* freqPower).sum()/ freqPower.sum()

        upFreq=int(self._mean+math.sqrt(self._std) * 3)
        self._freqPower = freqPower[0:(len(freqPower), upFreq)[upFreq<len(freqPower)]]

        logger.debug("mean:" + str(self.mean()))
        logger.debug("std: {}".format(
            str(self.std())
        ))
        logger.debug("freq: {} ± {}".format(
            str(self.mean()),
            str(abs(sqrt(self.std())))
        ))

    def getFreqPower(self):
        return self._freqPower

    def _geDftHalf(self,dftData):
        return np.absolute(dftData[1:len(dftData) / 2])

    def mean(self):
        return self._mean

    def std(self):
        return self._std

"""
def calculateFreqDeviationDependences():
    seed = 666
    lambda_val = 0.02
    devider = 200
    k_eff = -1
    h = [1.2, 0, 0]
    start = [1, 2, 2]

    dt=0.02
    establishedTime=400
    mesureTime=10000

    establishedLenght=int((float(establishedTime)/dt)/devider)
    mesureLenght=int((float(mesureTime)/dt)/devider)
    logger.debug("establishedLenght:" + str(establishedLenght))
    logger.debug("mesureLenght:" + str(mesureLenght))

    Dh_vals = [10 ** -i for i in range(1, 4)]
    Jalpha_vals=[float(i)/100 for i in range(-50, -45, 5)]

    for Dh in Dh_vals:
        for Jalpha in Jalpha_vals:
            J = lambda_val * Jalpha
            spingen = SpingenDescartesExternal(
                lambda_val = lambda_val,
                J = J,
                k_eff = k_eff,
                h = h,
                dt = dt,
                start = start,
                devider = devider,
            )
            random = RandomNormalExternal(mean = 0, deviation = Dh, seed = seed);
            spingenCalculate.putDataToCalc(spingen, random, establishedLenght, mesureLenght)
    result=spingenCalculate.calculate(calcFreqDeviation)

    np.array_split(result,len(Dh_vals))
"""

#def main():
#    dftResult=np.fft.fft(np.swapaxes(spingen.getTestSpingenNoisedData(),0,1))
#    meanFreq=getMeanFreq(dftResult)
#    stdFreq=getStdFreq(dftResult)
#    graph.

def autocorr(data):
    dataFT = np.fft.fft(data)
    dataAC = np.fft.ifft(dataFT * np.conjugate(dataFT)).real
    return dataAC

if __name__ == "__main__":
    import graph
    logger.debug("Freq deviation test")
    data=np.array(range(100000))*2*math.pi/100
    data=np.sin(data) #+ np.sin(data/5) + np.sin(data*5)
    print FreqParams(data).mean()
    graph.showDft(data)