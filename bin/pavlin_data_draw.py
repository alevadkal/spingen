#coding=utf8
import numpy as np
import graph
import logger
logger = logger.logger(__name__,logger.debug)

class PavlinDataDrawer:
    beta = -1;
    h = 1.2;
    alpha = 0.02;
    ja=1.2;
    x=np.linspace(0,np.pi,10000)
    def __init__(self,DhList,DhDataList):
        self.DhList=DhList
        self.DhDataList=DhDataList
    def getTheoryData(self,Dh):
        return np.exp(
            (self.alpha/(self.beta*Dh))*(
                ((self.ja - self.h - self.beta * np.cos(self.x))**2)
            )
        );
    def draw(self):
        for Dh in self.DhList:
            graph.show2d(self.x,self.getTheoryData(Dh))

def main():
    PavlinDataDrawer([0.1,0.01,0.001,0.0001],None).draw()

if __name__ == "__main__":
    main()