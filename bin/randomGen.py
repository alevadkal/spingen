#! /bin/python
#coding=utf8

import sys
import os
from os.path import dirname
from subprocess import Popen, PIPE
import logger, logging
logger = logger.logger('random',logging.DEBUG)


class RandomNormalExternal:
    _cmd = (
        '{rand_normal}' +
        ' --mean {mean}' +
        ' --deviation {deviation}' +
        ' --seed {seed}'
    )

    def __init__(self,mean,deviation,seed = 0,debug = False):
        self._cmd = self._cmd.format(
            rand_normal = os.path.join(dirname(sys.argv[0]),'rand_normal'),
            seed = seed,
            mean = mean,
            deviation = deviation,
        )
        self._createdProcess = None
        if debug:
            self._cmd += ' --debug'
        self.debug=debug

    def _createProcess(self, dataLength = None):
        if(self._createdProcess!=None):
            logger.error("Process can be created only once!");
            raise RuntimeError
        if(dataLength != None):
            self._cmd += ' --length ' + str(dataLength)
        try:
            logger.info("Start cmd:" + self._cmd)
            self._createdProcess=Popen(self._cmd, stdout = PIPE, shell = True)
        except OSError,e:
            logger.error("Can't run:"+ self._cmd)
            logger.error("Exception:"+ str(e))
            raise RuntimeError

    def getPipe(self):
        self._createProcess();
        return self._createdProcess.stdout;

    def getData(self, dataLength):
        self.createProcess(dataLength);
        return [float(line) for line in self._createdProcess.stdout]

    def __del__(self):
        if(self._createdProcess != None):
            self._createdProcess.kill()

def main():
    print 'Test'
    print 'Genrate 10 values for RandomNormalExternal:'
    print RandomNormalExternal(1,0.1,666,debug=True).getData(10)

if __name__ == "__main__":
    main()