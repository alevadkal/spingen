#! /bin/python
#coding=utf8

import multiprocessing

import logger, logging
"""
    Небольшой модуль предназначенный для поставки пакетных заданий.
    p.s. не уверен что будет работать под виндой. 
"""
logger = logger.logger('spingen_calc',logging.DEBUG)


_calculateList = []

def putDataToCalc(calcObject):
    """
        calcObject должен содержать среди своих членов метод calc
        именно этот метод доложен возвращать данные которые будут получены из процесса.
    """
    global _calculateList
    _calculateList.append(calcObject)

def _calc(index):
    global _calculateList
    return _calculateList[index].calc()

def calculate():
    """
        Запускает процесс расчёта.
    """
    global _calculateList
    numberOfTasks=len(_calculateList)
    logger.info("Start calculate for " + str(numberOfTasks) + " tasks")
    processPool = multiprocessing.Pool(multiprocessing.cpu_count()) #расёт производится для каждого процессора.
    result = processPool.map(_calc, range(numberOfTasks))
    _calculateList=[] #we end calculating. Delete old values
    logger.info("calculate DONE!")
    return result

def main():
    print("It's not tested while =)")

if __name__ == "__main__":
    main()