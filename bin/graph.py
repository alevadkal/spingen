#coding=utf8
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from  mpl_toolkits.mplot3d import Axes3D #@UnresolvedImport
import numpy as np

import logger, logging
logger = logger.logger('graph',logging.INFO)

def _initSpingenAxis(ax):
    ax.legend()

    ax.set_xlim3d([-1.0, 1.0])
    ax.set_xlabel('X')

    ax.set_ylim3d([-1.0, 1.0])
    ax.set_ylabel('Y')

    ax.set_zlim3d([-1.0, 1.0])
    ax.set_zlabel('Z')

    ax.set_title('Spingen')

def showSpingen3d(result,label='spingen'):
    result=np.swapaxes(result,0,1)
    x=result[0]
    y=result[1]
    z=result[2]
    ax = plt.gca(projection='3d')
    _initSpingenAxis(ax)
    ax.plot(x, y, z, label=label)
    ax.legend()
    plt.show()


class _SpingenAnimator:
    def __init__(self, plot, line, result,fps, val_per_frame, plum_time):
        """
            Анимация для спиновго генератора.
        """
        self.plot = plot
        self.line = line
        self.result = result
        self.interval = 1000 / fps

        self.val_per_frame = val_per_frame
        self.plum_values = int(plum_time * fps * val_per_frame)
        self.frames = len(result)/val_per_frame -1


        logger.debug("plum_values=" + str(self.plum_values))
        logger.debug("val_per_frame=" + str(self.val_per_frame))
        logger.debug("frames=" + str(self.frames))
        logger.info("Anumation length:" + str(float(self.frames)/fps))

        def anim(num, data):
            num += 1
            logger.debug("make frame "+ str(num))
            end = num * val_per_frame
            start = (end - data.plum_values, 0)[end - self.plum_values < 0]
            logger.debug("start:{start}; end:{end}".format(start=start, end=end))
            draw_data = np.swapaxes(data.result[start:end],0,1)
            logger.debug("length to draw {len}".format(len=len(data.result[0])))
            data.line.set_xdata(draw_data[0])
            data.line.set_ydata(draw_data[1])
            data.line.set_3d_properties(draw_data[2])
            return [data.line]
        logger.info("start Animation!")
        self.animation = animation.FuncAnimation(self.plot, anim, self.frames ,interval=25, blit=True,  fargs=([self]))

def showDft(result, dt=0.01, devider=1, scale=1):
    """
        Строит одномерное преобразование Фурье для полученных данных
    """
    from freqDeviation import FreqParams
    freqParams = FreqParams(result)
    freqPower = freqParams.getFreqPower()
    length = len(freqPower)
    freqPower = np.hstack((np.flipud(freqPower[1:]), freqPower,0))
    x = np.array(range(-length+1,length+1,1))
    _, ax = plt.subplots()
    ax.plot(x, freqPower, alpha=0.3)
    plt.show()

def show2d(x,y):
    """
        Метод для отладки - построение двух мерных граффиков.
    """
    _, ax = plt.subplots()
    ax.plot(x, y, alpha=0.3)
    plt.show()

def show1d(x):
    """
        Метод для отладки - построение граффика одномерной последовательности.
    """
    _, ax = plt.subplots()
    ax.plot(x, alpha=0.3)
    plt.show()

def showSpingen3dAnimation(result, label = 'spingen',fps=25, val_per_frame=10, plum_time=10):
    """
        метод строит анимацию трёхмерного граффика по полученным данным
    """
    fig = plt.figure()
    ax = Axes3D(fig)
    _initSpingenAxis(ax)
    line, = ax.plot([1], [0], [0], label='spingen')
    animation = _SpingenAnimator(fig, line, result, fps=fps, val_per_frame=val_per_frame, plum_time=plum_time)
    plt.show()