#! /bin/python
#coding=utf8

import os
import sys
from os.path import dirname
from subprocess import Popen, PIPE
import numpy as np
import graph
from randomGen import RandomNormalExternal


import logger, logging
logger = logger.logger('spingen',logging.DEBUG)

class SpingenDescartesExternal:
    _cmd=(
        '{spin_descartes}' +
        ' --lambda {lambda_val}' +
        ' --J {J}' +
        ' --k_eff {k_eff}'+
        ' --h {h}' +
        ' --dt {dt}' +
        ' --start {start}' +
        ' --devider {devider}' +
        ' --established_time {established_time}'
    )

    def __init__(
        self,
        lambda_val = 0.02,
        J = 0.035,
        k_eff = -1,
        h = [1.2, 0, 0],
        dt = 0.001,
        start = [1, 2, 2],
        devider = 1,
        establishedTime = 0,
        integrateTime = None,
        debug=False,
    ):
        self.debug = debug
        self._devider=devider
        self._cmd = self._cmd.format(
            spin_descartes = os.path.join(dirname(sys.argv[0]), 'spin_descartes'),
            lambda_val = lambda_val,
            J = J,
            k_eff = k_eff,
            h = ','.join([str(val) for val in h[0:3]]),
            dt = dt,
            start = ','.join([str(val) for val in start[0:3]]),
            devider = devider,
            established_time = establishedTime
        )
        self._createdProcess = None
        if debug: self._cmd += ' --debug'
    
    def _createProcess(self, random, integrateTime = None):
        if(self._createdProcess != None):
            logger.error("Process can be created only once!");
            raise RuntimeError
        cmd = self._cmd
        if(integrateTime != None):
            cmd = self._cmd + " --integrate_time " + str(integrateTime)
        logger.debug("Start cmd:" + cmd)
        self._createdProcess = Popen(
            cmd,
            stdin = random.getPipe(),
            stdout = PIPE, shell=True
        )

    
    def getPipe(self, random, integrateTime = None):
        '''
        Получает pipe с stdout спиновго генератора.

        random          генератор имеющий метод getPipe, которая возвращает последовательность случайных чисел
        dataLength      колличестов измерени спиновго генератора, которые необходимо вернуть
        '''
        self._createProcess(random, integrateTime);
        return self._createdProcess.stdout;
    
    def getData(self, random, integrateTime):
        '''
        Получает данные из спиновго генератора
        random - генератор шума. Должен иметь метод getPipe
        integrateTime время интегрирования
        '''
        if not type(integrateTime) in [float, int]:
            raise ValueError
        data = np.array([
            np.array([
                float(value) for value in line.strip().split(' ')
            ]) for line in self.getPipe(random, integrateTime).readlines()
        ])
        return data
    
    def __del__(self):
        if(self._createdProcess != None):
            self._createdProcess.kill()


def getTestSpingenNoisedData(integrateTime, establishedTime = 0, devider = 1, dt = 0.001):
    random=RandomNormalExternal(0,0.01,666)
    spingen = SpingenDescartesExternal(
        lambda_val = 0.02,
        J = 0.032,
        k_eff = -1,
        h = [1.2, 0, 0],
        dt = dt,
        start = [1 ,2 ,2],
        devider = devider,
        establishedTime = establishedTime,
    )
    logger.debug('start calc')
    return spingen.getData(random, integrateTime)



def main():
    def showDft(result):
        result=np.swapaxes(result,0,1)[0]
        graph.showDft(result)

    logger.info('Test')
    logger.info('Genrate values for SpingenDescartesExternal with RandomNormalExternal as random generator:')

    result = getTestSpingenNoisedData(1000, establishedTime = 50, devider = 20, dt = 0.001)
    logger.debug('done calc')

    if(len(sys.argv) == 1):
        logger.debug('show 3d graph for spingen data')
        graph.showSpingen3dAnimation(result)
        return
    if(sys.argv[1] == 'dft'):
        showDft(result)
        return
    print result

if __name__ == "__main__":
    main()